/**
 * @param items
 * @param cb
 * @returns {Promise<*>}
 */
module.exports = async (items, cb = null) =>
  items.reduce(
    (promise, item) =>
      /* Chain one or more promises, one after the other */
      promise.then(last => {
        /* Get the last result, in order to pass this to the next chained promise */
        const lastResult = last.slice(-1).pop();
        /* Determine the next promise callback */
        const nextPromise = cb ? cb(item, lastResult) : item(lastResult);
        /* Call the next promise, appending results with that of the next promise */
        return nextPromise.then(result => [...last, result]);
      }),
    /* Initial promise state */
    Promise.resolve([])
  );
