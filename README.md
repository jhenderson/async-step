# async-step

A basic ES6 async/await chain function

 This basic function allows chaining multiple promises/async functions.
 Two possible styles are supported.
 Note: Each callback will be called with the next input parameter, and the last result.

 1. Single callback - Use this if every item has the same callback
   ```
   const results = await asyncStep([0, 1, 2], cb);
   // results will contain the output of each cb() execution
   console.log(results);
   ```
 2. Many callbacks - Use this if you need configurable callbacks per function
   ```
   const results2 = await forEach([
    () => cb(0),
    last => cb(1, last),
    last => cb(2, last),
   ]);
   ```

